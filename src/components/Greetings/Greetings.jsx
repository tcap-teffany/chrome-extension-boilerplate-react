import React from 'react';
import icon from '../../assets/img/icon128.png';

const GreetingComponent = () => {
  const name = 'dev'

  return (
    <div>
      <p>Hello, {name}!</p>
      <img src={icon} alt="extension icon" />
    </div>
  );
};

export default GreetingComponent;
