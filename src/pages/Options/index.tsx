import React from 'react';
import { render } from 'react-dom';

import './index.css';
interface Props {
  title: string;
}

const Options: React.FC<Props> = ({ title }: Props) => {
  return <div className="OptionsContainer">{title.toUpperCase()} PAGE</div>;
};

render(
  <Options title={'settings'} />,
  window.document.querySelector('#app-container')
);
