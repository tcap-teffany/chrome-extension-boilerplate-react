import React, {useState} from 'react';
import { render } from 'react-dom';
import Greetings from '../../components/Greetings/Greetings';
import './index.css';
import user from '../../assets/img/user.svg';
import automatic from '../../assets/img/automatic.svg';
import manual from '../../assets/img/manual.svg';
import info from '../../assets/img/info.svg';
import manualActive from '../../assets/img/manualActive.svg';

const Popup = () => {
  const [iconActive, setIconActive] = useState(manual);
  window.onload = () => {
    console.log('started popup');
    mainView = document.getElementById('mainPopup');
    mainView.classList.add('hidden');
    // var readyStateCheckInterval = setInterval(function () {
    //     if (document.readyState === "complete") {
    //         clearInterval(readyStateCheckInterval);
    //
    //         // ----------------------------------------------------------
    //         // This part of the script triggers when page is done loading
    //         // ----------------------------------------------------------
    //         init = true;
    //         console.log("started site editor");
    //     }
    // }, 10);
    //check liveness
    checkAlive();
    activateItem();
  };
  function activateItem(){
    var act = document.getElementsByTagName("A");
    for (var i = 0; i < act.length; i++) {
      act[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active");
        if (current.length > 0) { 
          current[0].className = current[0].className.replace(" active", "");
        }
        this.className += " active";
        console.log('test');
      });
    }
  }

  var mainView, currentTab;
  var stateButton;

  function initButtons(active) {
    stateButton = document.getElementById('change-state');
    if (active) {
      setIconActive(manualActive);
    } else {
      setIconActive(manual);
    }
    stateButton.addEventListener('click', () => {
      chrome.tabs.sendMessage(
        currentTab.id,
        { currentSetting: { enableExtension: !active } },
        function (response) {}
      );

      window.close();
    });
  }

  function checkAlive() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      currentTab = tabs[0];
      chrome.tabs.sendMessage(
        currentTab.id,
        { checkAlive: true, currentWindow: true },
        function (response) {
          if (response && response.alive) {
            mainView.classList.remove('hidden');
            document.getElementById('loader').classList.add('hidden');
            initButtons(response.active);
          } else {
            setTimeout(checkAlive, 1000);
          }
        }
      );
    });
  }

  return (
    <>
      <div id="loader" class="container spinner-container">
        <div>
          Please wait until page finishes loading <i class="spinner">/</i>
        </div>
      </div>
      <div id="mainPopup" class="container">
        <ul class="toolIcons">
          <li title="User">
            <a href="#">
              <img src={user} alt />
            </a>
          </li>
          <li title="Automatic">
            <a href="#">
              <img src={automatic} alt />
            </a>
          </li>
          <li id="change-state" title="Manual">
            <a href="#">
              <img src={iconActive} alt />
            </a>
          </li>
          <li title="Info">
            <a href="#">
              <img src={info} alt />
            </a>
          </li>
        </ul>
      </div>
    </>
  );
};

render(<Popup />, window.document.querySelector('#app-container'));
